#!/bin/sh

set -e

envsubst < /etc/nginx/default.com.tpl > /etc/nginx/conf.d/default.conf
nginx -g 'daemon off;'